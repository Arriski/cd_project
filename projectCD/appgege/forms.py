from django import forms
from .models import Table
from datetime import datetime

class FormTable(forms.ModelForm):
    box = forms.CharField(widget=forms.TextInput(attrs={'type':'text'}),required=True)

    class Meta:
        model = Table
        fields = ('box',)

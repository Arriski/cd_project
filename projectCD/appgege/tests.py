from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import TableTennis
from .models import Table
from .forms import FormTable
# Create your tests here.

class appgegeUnitTest(TestCase):
    
    def test_lab_6_url_is_exist(self):
        response = Client().get('/appgege/')
        self.assertEqual(response.status_code, 200)

    def test_lab5_using_index_func(self):
        found = resolve('/appgege/')
        self.assertEqual(found.func, TableTennis)
     
    def test_form_todo_input_has_placeholder_and_css_classes(self):
        response = Client().get("/appgege/")
        response_html = response.content.decode('utf8')
        self.assertContains(response, 'class="container"')

    def test_appgege_post_success_and_render_the_result(self):
        test = 'anonymous'
        response_post = Client().post('/appgege', {'status': test})
        self.assertEqual(response_post.status_code, 200)
        response= Client().get('/appgege/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)
        
    def test_appgege_post_error_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/appgege/', {'status': ''})
        self.assertEqual(response_post.status_code, 200)
        response= Client().get('/appgege/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)
        
    def test_form_validation_for_blank_items(self):
        form = FormTable(data={'box': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(form.errors['box'],["This field is required."])        

    def test_model_can_create_box(self):
        # Creating a new activity
        new_box = Table.objects.create(box='mengerjakan lab ppw')
        # Retrieving all available activity
        count_all_variable = Table.objects.all().count()
        self.assertEqual(count_all_variable, 1)

    def test_appgege_contains_hello(self):
        response = Client().get('/appgege/')
        response_html = response.content.decode('utf8')
        self.assertContains(response, 'Halo, apa kabar?')


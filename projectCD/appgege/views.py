from django.shortcuts import render
from .models import Table
from .forms import *
# Create your views here.

def TableTennis(request):
    if (request.method == "POST"):
        form = FormTable(request.POST or None)
        if form.is_valid():
            print(request.POST)
            box = request.POST["box"]
            post = Table(box=box)
            post.save()
    else:
        
        form = FormTable()
    all_table = Table.objects.all()
    print(all_table)
    return render(request, 'StatusHTML.html', {'Table': all_table,'form':form})

def StatusHTML(request):
    all_table = Table.objects.all()
    html = 'StatusHTML.html'
    print(all_table)
    return render(request, html, {'Table':all_table})

from django.db import models
from datetime import datetime

# Create your models here.
class Table(models.Model):
    date = models.DateTimeField(default = datetime.now)
    box = models.CharField(max_length=300)

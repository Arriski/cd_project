from django.urls import re_path
from .views import TableTennis, StatusHTML
#url for app
urlpatterns = [
    re_path(r'^$', TableTennis, name='table'),
    re_path('table', StatusHTML, name = 'status') 
]
